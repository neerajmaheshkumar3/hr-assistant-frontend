import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  QuestionForm!: FormGroup;
  query: string;
  response: any

  title = 'hr-assistant';

  constructor(private httpClient: HttpClient, private fb: FormBuilder) {
    this.query='';
    this.formgroupvalid(); 
    };
  
  

  ngOnInit(){}
  

  public formgroupvalid()
     {
      this.QuestionForm = this.fb.group
      (
        {
        query: ['', [Validators.required]],
        }
      );

      }

     

  submit()
  {
       this.query=this.QuestionForm.get('query')?.value; 
       console.log(this.query)
       this.sendQuery();
       this.getResponse();

  }


  sendQuery() 
  {
    this.httpClient.post("http://127.0.0.1:5002/question", this.query).subscribe(response=> {
      console.log('Data sent successfully!', this.query)});
  }
  
  getResponse() 
  {
      this.httpClient.get("http://127.0.0.1:5002/response",{ responseType: 'text' }).subscribe((data) => {
      this.response = data.toString()
      console.log(data);});
  }
    

  

}